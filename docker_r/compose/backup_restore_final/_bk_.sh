#!/bin/bash
docstring='
BACKUP_FROM_C=rr_db_c--backuprestoredb ./_bk_.sh
'
SH=$(cd `dirname $BASH_SOURCE` && pwd)

[ -z $BACKUP_FROM_C ] && (echo 'Envvar BACKUP_FROM_C is required' ; kill $$)

rootatrr='root@prototype.referreach.com'

    # preapre remote temp folder
    rTEMP='/tmp/rr.backuprestoredb'  # remote temp folder
        ssh "$rootatrr" mkdir -p "$rTEMP"

    rootatrr='root@prototype.referreach.com'
        # upload script to remote
        rsync -chaz "$SH/run_bk_on_remote.sh" "$rootatrr:$rTEMP/run_bk_on_remote.sh"

            # run script on remote
            log_f="$SH/.backup/log"
            ssh "$rootatrr"  "BACKUP_FROM_C=$BACKUP_FROM_C  $rTEMP/run_bk_on_remote.sh" | tee $log_f
            #   .             !                             ! backup file be stored at $rTEMP/.backup/

            # download backup file
            remote_backup_f=$(tail -n1 $log_f) ; fn=$(basename -- $remote_backup_f)
            echo "Downloading $remote_backup_f"
                scp "$rootatrr:$remote_backup_f"  "$SH/.backup/$fn"

echo -e "\n\n--- Backup file ready at \n$(ls -lah $SH/.backup/$fn)"
