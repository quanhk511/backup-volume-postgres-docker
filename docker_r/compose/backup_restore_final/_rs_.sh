#!/bin/bash
docstring='
RESTORE_FROM=backup-20210316_153420.tar RESTORE_TO=path/to/restore ./_rs_.sh
'
SH=$(cd `dirname $BASH_SOURCE` && pwd)

[ -z $RESTORE_FROM ] && (echo 'Envvar RESTORE_FROM is required' ; kill $$)    # file .tar
[ -z $RESTORE_TO   ] && (echo 'Envvar RESTORE_TO is required' ; kill $$)      # path to vol

# del all data
sudo rm -rf $RESTORE_TO/*
# decompression
sudo tar xvf $SH/.backup/$RESTORE_FROM -C $RESTORE_TO