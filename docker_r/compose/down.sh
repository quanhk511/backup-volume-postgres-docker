#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/.." && pwd)
set -a
    source "$SH/.compose.env"
    source "$AH/containeratruntime.env" # env runtime for db use env for webapp

        docker-compose -p $COMPOSE_PROJECT  -f "$SH/docker-compose.yml"  down > /dev/null 2>&1 || true
