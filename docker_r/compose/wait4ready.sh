#!/bin/bash

SH=$(cd `dirname $BASH_SOURCE` && pwd)

source "$SH/.compose.env"

echo; printf "Wait until the postgres ready ..."
        while true; do

           check=`docker ps --format '{{.Names}} {{.Status}}' | grep "$C_NAME_DB" | grep -c "Up" --color=always `
           
            [[ "$check" == "1"  ]] && break
               
                printf '.'; sleep 1
                    done
            echo 'Done'
            