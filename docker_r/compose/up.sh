#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/.." && pwd)
set -a
    source "$SH/.compose.env"
    $SH/down.sh
    source "$AH/containeratruntime.env" # env runtime for db use env for webapp

        docker-compose  -p $COMPOSE_PROJECT -f "$SH/docker-compose.yml"  up  -d 

        sleep 5
        $SH/wait4ready.sh

        docker ps --format 'Name: {{.Names}} | Ports: {{.Ports}} | Status: {{.Status}}'
        
        echo ; echo 'serial id in postgres'
        $SH/backup_restore_final/aftermath.sh
