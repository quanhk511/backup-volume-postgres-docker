#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/../.." && pwd)

source "$SH/../.compose.env"
source "$AH/containeratruntime.env"
echo
    docker exec -it $C_NAME_DB psql "postgresql://$DB_USER:$DB_PASS@$DB_HOST/$DB_NAME"  -c 'select id from users'