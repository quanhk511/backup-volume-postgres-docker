#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/.." && pwd)
TIME=`date +%Y%m%d_%H%M%S`

docstring='
cd docker_r/compose/backup_db/
C_BACKUP="db_rr" ./backup.sh
'
[ -z $C_BACKUP ] && (echo 'Envvar C_RESTORE is required' ; kill $$) # name container's volume you want backup

DIR_COTAIN_TAR="$SH/tar"
FILE_TAR="backup-$TIME.tar"		

		docker run -dit \
				   --name backup \
				   --volumes-from $C_BACKUP `# load volume in :C_BACKUP to this container ie will have $DEFAULT_POSGRES_DATA_DIR` \
				   -v $DIR_COTAIN_TAR:/backup ubuntu

		DEFAULT_POSGRES_DATA_DIR='/var/lib/postgresql/data/'
		docker exec -it backup /bin/bash -c " cd $DEFAULT_POSGRES_DATA_DIR && \
		 					        	       tar cvf /backup/$FILE_TAR ."


		docker rm -f backup > /dev/null 2>&1
