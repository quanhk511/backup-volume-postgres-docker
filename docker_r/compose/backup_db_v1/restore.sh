#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/.." && pwd)

docstring='
cd docker_r/compose/backup_db/
C_RESTORE="db_rr" FILE_TAR="backup-01.tar" ./restore.sh
'
[ -z $C_RESTORE ] && (echo 'Envvar C_RESTORE is required' ; kill $$) # name container's volume you want restore
[ -z $FILE_TAR ] && (echo 'Envvar FILE_TAR is required' ; kill $$) # name file.tar's

DIR_COTAIN_TAR="$SH/tar"		

		docker run -dit \
				   --name restore \
				   --volumes-from $C_RESTORE \
				   -v $DIR_COTAIN_TAR:/restore ubuntu

		docker exec -it restore /bin/bash -c " cd /restore && \
		 					        	 	   rm -rf /var/lib/postgresql/data/* && \
		 					        	       tar xvf $FILE_TAR -C /var/lib/postgresql/data"

		
		docker rm -f restore > /dev/null 2>&1
		
		source "$AH/restart.sh" # restart ctn
		sleep 6

		echo; echo 'serial id in postgres'
