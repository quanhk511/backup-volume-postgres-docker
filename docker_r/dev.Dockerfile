FROM namgivu/ubuntu-pipenv:20.04-3.8

WORKDIR /app

COPY . .

RUN pipenv sync

COPY _run/.env.template  ./_run/.env
COPY _whitelist.tpl.py   ./_whitelist.py


EXPOSE 5000

CMD PYTHONPATH=`pwd`  pipenv run  gunicorn  -b "0.0.0.0:5000"  src.app:app    --reload   --timeout 666   --log-level debug   2>&1  $*    | tee -a $AH/tmp/log
