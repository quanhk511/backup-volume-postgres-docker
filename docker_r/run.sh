#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
source "$SH/.run_app.env"
source "$SH//stop.sh"
docker run -d \
    --env-file "$SH/containeratruntime.env" \
    --network=$NET_WORK \
    --name $C_NAME  -p $PORT_WEB_APP:5000 $IMG_TAG
