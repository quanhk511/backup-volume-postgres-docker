#!/bin/bash
SH=$(cd `dirname $BASH_SOURCE` && pwd)
AH=$(cd "$SH/.." && pwd)

source "$SH/.run_app.env"
    docker build  --no-cache  --file "$SH/dev.Dockerfile"  -t $IMG_TAG  $AH
